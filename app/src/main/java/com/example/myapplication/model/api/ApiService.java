package com.example.myapplication.model.api;


import com.example.myapplication.model.Request;
import com.example.myapplication.model.User;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ApiService {


    @GET("get_user")
    Single<User> getUserDetail();


    @POST("new_request")
    Single<Request> sendRequest();
}
