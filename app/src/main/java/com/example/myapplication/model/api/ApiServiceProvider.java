package com.example.myapplication.model.api;

public class ApiServiceProvider {

    private static ApiService apiService;

    public static ApiService provideApiService(){
        if (apiService == null) {
            apiService = RetrofitSingleton.getInstance().create(ApiService.class);
        }
        return apiService;

    }

}
