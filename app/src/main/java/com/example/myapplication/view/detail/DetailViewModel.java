package com.example.myapplication.view.detail;

import com.example.myapplication.base.BaseViewModel;
import com.example.myapplication.model.Request;
import com.example.myapplication.model.User;

import io.reactivex.Single;

public class DetailViewModel extends BaseViewModel {

    DetailViewModel() {
        initViewModel();
    }

    Single<Request> sendRequest() {
        return apiService.sendRequest();
    }
}
