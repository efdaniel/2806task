package com.example.myapplication.base;

import androidx.lifecycle.ViewModel;

import com.example.myapplication.model.api.ApiService;
import com.example.myapplication.model.api.ApiServiceProvider;

public class BaseViewModel extends ViewModel {
    public static ApiService apiService;



    public void initViewModel(){
        apiService = ApiServiceProvider.provideApiService();
    }
}
